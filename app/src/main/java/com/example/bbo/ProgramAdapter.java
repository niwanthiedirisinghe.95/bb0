package com.example.bbo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class ProgramAdapter extends ArrayAdapter<String> {

    Context context;
    int[] images;
    String[] titles;

    public ProgramAdapter(Context context, String[] titles, int[] images){
        super(context,R.layout.single_item, R.id.textView6,titles);
        this.context = context;
        this.images = images;
        this.titles = titles;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        View singleItem = convertView;
        ProgramViewHolder holder = null;
        if(singleItem == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            singleItem = layoutInflater.inflate(R.layout.single_item,parent,false);
            holder = new ProgramViewHolder(singleItem);
            singleItem.setTag(holder);
        }
        else{
            holder = (ProgramViewHolder) singleItem.getTag();
        }
        holder.imageView.setImageResource(images[position]);
        holder.title.setText(titles[position]);
        singleItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"BBO is turning towards to "+ titles[position],Toast.LENGTH_SHORT).show();
            }
        });
        return singleItem;
    }
}
