package com.example.bbo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

public class StarActivity extends AppCompatActivity {

    ListView starList;

    String[] starName = {"Libra","Arrow","Little Bear","Berenices Hair","Cancer","Scorpio","Aurigo","Phoenix","Volans","Aries"};
    int[] starImages = {R.drawable.libra,R.drawable.arrow,R.drawable.littlebear,R.drawable.bereniceshair,
            R.drawable.cancer,R.drawable.scorpio,R.drawable.auriga,R.drawable.phoenix,R.drawable.volans,R.drawable.aries};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_star);

        starList = findViewById(R.id.star);
        ProgramAdapter programAdapter =  new ProgramAdapter(this,starName,starImages);
        starList.setAdapter(programAdapter);
    }
}
