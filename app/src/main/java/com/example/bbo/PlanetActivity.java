package com.example.bbo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

public class PlanetActivity extends AppCompatActivity {

    ListView planetList;

    String[] planetName = {"Jupiter","Mars","Mercury","Venus","Saturn"};
    int[] planetImages = {R.drawable.jupiter,R.drawable.mars, R.drawable.mercury,R.drawable.venus,R.drawable.saturn};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planet);

        planetList = findViewById(R.id.planet);
        ProgramAdapter programAdapter =  new ProgramAdapter(this,planetName,planetImages);
        planetList.setAdapter(programAdapter);
    }
}
