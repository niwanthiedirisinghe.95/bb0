package com.example.bbo;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ProgramViewHolder {

    ImageView imageView;
    TextView  title;

    public ProgramViewHolder(View v){
        imageView = v.findViewById(R.id.imageView);
        title = v.findViewById(R.id.textView6);
    }
}
